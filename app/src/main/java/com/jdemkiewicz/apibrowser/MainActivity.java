package com.jdemkiewicz.apibrowser;

import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.jdemkiewicz.apibrowser.Fragments.FavoritesFragment;
import com.jdemkiewicz.apibrowser.Fragments.FightersFragment;
import com.jdemkiewicz.apibrowser.Fragments.HomeFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    @BindView(R.id.bottom_menu)
    BottomNavigationView bottomNavigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        bottomNavigationView.setOnNavigationItemSelectedListener(this);
        displayFragment(HomeFragment.newInstance());

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        Fragment selectedFragment = getFragment(item);
        displayFragment(selectedFragment);
        return true;
    }

    private Fragment getFragment(@NonNull MenuItem item) {
        Fragment selectedFragment = null;
        switch (item.getItemId()) {
            case R.id.home_menu_item:
                selectedFragment = HomeFragment.newInstance();
                break;
            case R.id.fighters_menu_item:
                selectedFragment = FightersFragment.newInstance();
                break;
            case R.id.favorites_menu_item:
                selectedFragment = FavoritesFragment.newInstance();
                break;
        }
        return selectedFragment;
    }

    private void displayFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_container, fragment);
        fragmentTransaction.commit();
    }
}
