package com.jdemkiewicz.apibrowser.Api;

import com.jdemkiewicz.apibrowser.Models.Fighter;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by j.demkiewicz on 23.08.2017.
 */

public interface ApiService {
    @GET("/api/v1/us/fighters")
    Call<List<Fighter>> getFighters();

    @GET("/api/v1/us/fighters/{id}")
    Call<Fighter> getFighter(@Path("id") int fighterId);
}
