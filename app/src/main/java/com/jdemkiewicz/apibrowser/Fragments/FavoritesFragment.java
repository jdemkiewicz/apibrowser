package com.jdemkiewicz.apibrowser.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jdemkiewicz.apibrowser.Adapters.FavoriteAdapter;
import com.jdemkiewicz.apibrowser.Adapters.FightersAdapter;
import com.jdemkiewicz.apibrowser.Models.Fighter;
import com.jdemkiewicz.apibrowser.R;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by j.demkiewicz on 22.08.2017.
 */

public class FavoritesFragment extends Fragment {
    @BindView(R.id.favorites_container)
    RecyclerView recyclerView;
    @BindView(R.id.favorites_emptyMessage)
    TextView favEmptyText;


    private FavoriteAdapter adapter;
    private RealmResults<Fighter> favFighters;

    public static FavoritesFragment newInstance() {
        FavoritesFragment fragment = new FavoritesFragment();
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites, container, false);
        ButterKnife.bind(this, view);
        getFavoriteList();
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        return view;
    }

    private void getFavoriteList() {
        Realm realm = Realm.getDefaultInstance();
        favFighters = realm.where(Fighter.class).equalTo("favorite", true).findAll();
        if (favFighters.size() > 0) {
            refreshAdapter(favFighters);
            setLayoutVisibility(View.GONE, View.VISIBLE);
        }
    }

    private void setLayoutVisibility(int emptyTextVisibility, int recyclerViewVisibility) {
        favEmptyText.setVisibility(emptyTextVisibility);
        recyclerView.setVisibility(recyclerViewVisibility);
    }

    private void refreshAdapter(List<Fighter> fightersRealm) {
        adapter = new FavoriteAdapter(fightersRealm, getActivity());
        recyclerView.setAdapter(adapter);
    }
}
