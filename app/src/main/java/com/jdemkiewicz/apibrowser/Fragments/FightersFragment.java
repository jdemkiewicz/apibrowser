package com.jdemkiewicz.apibrowser.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.jdemkiewicz.apibrowser.Adapters.FightersAdapter;
import com.jdemkiewicz.apibrowser.Api.ApiService;
import com.jdemkiewicz.apibrowser.Models.Fighter;
import com.jdemkiewicz.apibrowser.R;
import com.jdemkiewicz.apibrowser.utils.RestClient;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by j.demkiewicz on 22.08.2017.
 */

public class FightersFragment extends Fragment implements android.widget.SearchView.OnQueryTextListener {
    private List<Fighter> fighters;

    @BindView(R.id.fighters_container)
    RecyclerView recyclerView;
    @BindView(R.id.edit_text)
    android.widget.SearchView searchView;
    @BindView(R.id.fragment_fighters_progressBar)
    ProgressBar progressBar;
    private FightersAdapter adapter;
    private Realm realm;

    public static FightersFragment newInstance() {
        return new FightersFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fighters = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_fighters, container, false);
        ButterKnife.bind(this, view);
        getFightersList();
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
        searchView.setOnQueryTextListener(this);
        return view;
    }

    private void getFightersList() {
        realm = Realm.getDefaultInstance();
        Retrofit retrofit = new RestClient().getRetrofit();
        ApiService apiService = retrofit.create(ApiService.class);
        apiService.getFighters().enqueue(new Callback<List<Fighter>>() {
            @Override
            public void onResponse(Call<List<Fighter>> call, Response<List<Fighter>> response) {
                if (response.isSuccessful()) {
                    fighters.addAll(response.body());
                    addOrUpdateToRealm();
                    RealmResults<Fighter> fightersRealm = realm.where(Fighter.class).findAll();
                    refreshAdapter(fightersRealm);
                    setLayoutVisibility();
                }
            }

            @Override
            public void onFailure(Call<List<Fighter>> call, Throwable t) {
                realm = Realm.getDefaultInstance();
                RealmResults<Fighter> fightersRealm = realm.where(Fighter.class).findAll();
                if (fightersRealm.size() > 0) {
                    refreshAdapter(fightersRealm);
                    setLayoutVisibility();
                } else {
                    Toast.makeText(getContext(), "Turn on internet connection on the first launch", Toast.LENGTH_SHORT).show();
                }
            }
        });
        realm.close();
    }

    private void addOrUpdateToRealm() {
        realm = Realm.getDefaultInstance();
        for (final Fighter fighter : fighters) {
            Fighter fighterQuery = realm.where(Fighter.class).equalTo("id", fighter.getId()).findFirst();
            if (fighterQuery != null) {
                fighter.setFavorite(fighterQuery.isFavorite());
            }
            realm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    realm.copyToRealmOrUpdate(fighter);
                }
            });
        }
    }

    private void setLayoutVisibility() {
        progressBar.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void refreshAdapter(List<Fighter> fightersRealm) {
        adapter = new FightersAdapter(fightersRealm, getActivity());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        adapter.getFilter().filter(s);
        return false;
    }
}
