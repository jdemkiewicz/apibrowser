package com.jdemkiewicz.apibrowser.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.jdemkiewicz.apibrowser.Api.ApiService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by j.demkiewicz on 23.08.2017.
 */


public class RestClient {
    private static final String BASE_URL = "http://ufc-data-api.ufc.com";
    private Retrofit retrofit;

    public RestClient() {
        Gson gson = new GsonBuilder().setLenient().create();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

    }

    public Retrofit getRetrofit() {
        return retrofit;
    }
}
