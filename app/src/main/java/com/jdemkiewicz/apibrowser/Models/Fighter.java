package com.jdemkiewicz.apibrowser.Models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import io.realm.FighterRealmProxy;
import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

/**
 * Created by j.demkiewicz on 22.08.2017.
 */
//@org.parceler.Parcel(implementations = {FighterRealmProxy.class},
////        value = Parcel.Serialization.BEAN,
//        analyze = {Fighter.class})
public class Fighter extends RealmObject implements Parcelable {

    @SerializedName("id")
    @Expose
    @PrimaryKey
    private Integer id;
    @SerializedName("wins")
    @Expose
    private Integer wins;
    @SerializedName("statid")
    @Expose
    private Integer statid;
    @SerializedName("losses")
    @Expose
    private Integer losses;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("weight_class")
    @Expose
    private String weightClass;
    @SerializedName("title_holder")
    @Expose
    private Boolean titleHolder;
    @SerializedName("draws")
    @Expose
    private Integer draws;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("fighter_status")
    @Expose
    private String fighterStatus;
    @SerializedName("thumbnail")
    @Expose
    private String thumbnail;

    @SerializedName("favorite")
    private boolean favorite;

    public Fighter() {
    }

    protected Fighter(Parcel in) {
        lastName = in.readString();
        weightClass = in.readString();
        firstName = in.readString();
        fighterStatus = in.readString();
        thumbnail = in.readString();
        favorite = in.readByte() != 0;
    }

    public static final Creator<Fighter> CREATOR = new Creator<Fighter>() {
        @Override
        public Fighter createFromParcel(Parcel in) {
            return new Fighter(in);
        }

        @Override
        public Fighter[] newArray(int size) {
            return new Fighter[size];
        }
    };

    public boolean isFavorite() {
        return favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @Override
    public String toString() {
        return "Fighter{" +
                "firstName='" + firstName + '\'' +
                ", favorite=" + favorite +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getWins() {
        return wins;
    }

    public void setWins(Integer wins) {
        this.wins = wins;
    }

    public Integer getStatid() {
        return statid;
    }

    public void setStatid(Integer statid) {
        this.statid = statid;
    }

    public Integer getLosses() {
        return losses;
    }

    public void setLosses(Integer losses) {
        this.losses = losses;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getWeightClass() {
        return weightClass;
    }

    public void setWeightClass(String weightClass) {
        this.weightClass = weightClass;
    }

    public Boolean getTitleHolder() {
        return titleHolder;
    }

    public void setTitleHolder(Boolean titleHolder) {
        this.titleHolder = titleHolder;
    }

    public Integer getDraws() {
        return draws;
    }

    public void setDraws(Integer draws) {
        this.draws = draws;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFighterStatus() {
        return fighterStatus;
    }

    public void setFighterStatus(String fighterStatus) {
        this.fighterStatus = fighterStatus;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(lastName);
        parcel.writeString(weightClass);
        parcel.writeString(firstName);
        parcel.writeString(fighterStatus);
        parcel.writeString(thumbnail);
        parcel.writeByte((byte) (favorite ? 1 : 0));
    }
}
