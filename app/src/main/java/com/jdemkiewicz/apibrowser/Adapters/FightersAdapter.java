package com.jdemkiewicz.apibrowser.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.jdemkiewicz.apibrowser.FighterDetailActivity;
import com.jdemkiewicz.apibrowser.Models.Fighter;
import com.jdemkiewicz.apibrowser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Case;
import io.realm.Realm;


/**
 * Created by j.demkiewicz on 22.08.2017.
 */

public class FightersAdapter extends RecyclerView.Adapter<FightersAdapter.FightersViewHolder> implements Filterable {
    private List<Fighter> fighters;
    private FragmentActivity activity;
    private List<Fighter> fightersFiltered;
    private Realm realm;

    public FightersAdapter(List<Fighter> fighters, FragmentActivity activity) {
        this.fighters = fighters;
        fightersFiltered = fighters;
        getFilter();

        this.activity = activity;
    }

    @Override
    public FightersViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fighters_card_layout, parent, false);
        return new FightersViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FightersViewHolder holder, final int position) {
        realm = Realm.getDefaultInstance();
        final Fighter fighter = fightersFiltered.get(position);
        holder.fighterNameText.setText(fighter.getLastName() + " " + fighter.getFirstName());
        holder.fighterStatusText.setText(fighter.getFighterStatus());
        resizeAndSetImage(holder, fighter);
        setFavoriteImage(holder, fighter);
        holder.favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!fighter.isFavorite()) {
                    setFavoriteOnClicked(holder, fighter, R.drawable.heart_pulse_red, true);
                } else {
                    setFavoriteOnClicked(holder, fighter, R.drawable.heart_pulse_dark, false);
                }
            }
        });
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity.getApplicationContext(), FighterDetailActivity.class);
//                intent.putExtra("fighter", fighter);
                intent.putExtra("image", fighter.getThumbnail());
                intent.putExtra("name", fighter.getFirstName());
                intent.putExtra("lastName", fighter.getLastName());
                intent.putExtra("wins", fighter.getWins());
                intent.putExtra("losses", fighter.getLosses());
                intent.putExtra("draws", fighter.getDraws());
                intent.putExtra("status", fighter.getFighterStatus());
                intent.putExtra("weight", fighter.getWeightClass());
                activity.getApplicationContext().startActivity(intent);
            }
        });
        realm.close();
    }

    private void setFavoriteImage(FightersViewHolder holder, Fighter fighter) {
        if (!fighter.isFavorite()) {
            holder.favorite.setImageResource(R.drawable.heart_pulse_dark);
        } else {
            holder.favorite.setImageResource(R.drawable.heart_pulse_red);
        }
    }

    private void setFavoriteOnClicked(FightersViewHolder holder, final Fighter fighter, int color, final boolean favorite) {
        realm = Realm.getDefaultInstance();
        holder.favorite.setImageResource(color);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                fighter.setFavorite(favorite);
            }
        });
        realm.close();
    }

    private void resizeAndSetImage(FightersViewHolder holder, Fighter fighter) {
        Picasso.with(holder.imageView.getContext())
                .load(fighter.getThumbnail())
                .resize(300, 300)
                .centerCrop()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return fightersFiltered.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                return new FilterResults();
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                String query = charSequence.toString().trim();
                realm = Realm.getDefaultInstance();
                if (query.isEmpty()) {
                    fightersFiltered = fighters;
                } else {
                    fightersFiltered = realm.where(Fighter.class)
                            .contains("firstName", query, Case.INSENSITIVE)
                            .or()
                            .contains("lastName", query, Case.INSENSITIVE)
                            .findAll();
                }
                notifyDataSetChanged();
                realm.close();
            }
        };
    }

    public static class FightersViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.cardview_fighters_favorite)
        ImageView favorite;

        @BindView(R.id.fighter_cardView)
        CardView cardView;

        @BindView(R.id.cardview_fighters_thumbnail)
        ImageView imageView;

        @BindView(R.id.cardview_fighters_name)
        TextView fighterNameText;

        @BindView(R.id.cardview_fighters_status)
        TextView fighterStatusText;

        public FightersViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
