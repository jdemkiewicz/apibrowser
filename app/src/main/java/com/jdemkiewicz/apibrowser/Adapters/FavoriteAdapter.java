package com.jdemkiewicz.apibrowser.Adapters;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.jdemkiewicz.apibrowser.FighterDetailActivity;
import com.jdemkiewicz.apibrowser.Models.Fighter;
import com.jdemkiewicz.apibrowser.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

/**
 * Created by j.demkiewicz on 24.08.2017.
 */

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder> {
    @BindView(R.id.favorites_container)
    RecyclerView recyclerView;
    @BindView(R.id.favorites_emptyMessage)
    TextView emptyMessage;
    private List<Fighter> fighters;
    private FragmentActivity activity;
    Realm realm;

    public FavoriteAdapter(List<Fighter> fighters, FragmentActivity activity) {
        this.fighters = fighters;
        this.activity = activity;
        realm = Realm.getDefaultInstance();
    }

    @Override
    public FavoriteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fighters_card_layout, parent, false);
        return new FavoriteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FavoriteViewHolder holder, final int position) {
        final Fighter fighter = fighters.get(position);
        holder.fighterNameText.setText(fighter.getLastName() + " " + fighter.getFirstName());
        holder.fighterStatusText.setText(fighter.getFighterStatus());
        resizeAndSetImage(holder, fighter);
        setFavoriteImage(holder, fighter);
        holder.favorite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFavoriteOnChanged(holder, fighter, R.drawable.heart_pulse_dark, false);
                notifyItemRemoved(position);
            }
        });
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(activity.getApplicationContext(), FighterDetailActivity.class);
//                intent.putExtra("fighter", fighter);
                intent.putExtra("image", fighter.getThumbnail());
                intent.putExtra("name", fighter.getFirstName());
                intent.putExtra("lastName", fighter.getLastName());
                intent.putExtra("wins", fighter.getWins());
                intent.putExtra("losses", fighter.getLosses());
                intent.putExtra("draws", fighter.getDraws());
                intent.putExtra("status", fighter.getFighterStatus());
                intent.putExtra("weight", fighter.getWeightClass());
                activity.getApplicationContext().startActivity(intent);
            }
        });
    }

    private void setFavoriteImage(FavoriteViewHolder holder, Fighter fighter) {
        holder.favorite.setImageResource(R.drawable.heart_pulse_red);
    }

    private void setFavoriteOnChanged(FavoriteViewHolder holder, final Fighter fighter, int color, final boolean favorite) {
        holder.favorite.setImageResource(color);
        realm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                fighter.setFavorite(favorite);
            }
        });
    }

    private void resizeAndSetImage(FavoriteViewHolder holder, Fighter fighter) {
        Picasso.with(holder.imageView.getContext())
                .load(fighter.getThumbnail())
                .resize(300, 300)
                .centerCrop()
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return fighters.size();
    }

    public static class FavoriteViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.cardview_fighters_favorite)
        ImageView favorite;

        @BindView(R.id.fighter_cardView)
        CardView cardView;

        @BindView(R.id.cardview_fighters_thumbnail)
        ImageView imageView;

        @BindView(R.id.cardview_fighters_name)
        TextView fighterNameText;

        @BindView(R.id.cardview_fighters_status)
        TextView fighterStatusText;

        public FavoriteViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}

