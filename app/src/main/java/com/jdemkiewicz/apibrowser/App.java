package com.jdemkiewicz.apibrowser;

import android.app.Application;

import io.realm.Realm;

/**
 * Created by j.demkiewicz on 22.08.2017.
 */

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Realm.init(this);
    }
}
