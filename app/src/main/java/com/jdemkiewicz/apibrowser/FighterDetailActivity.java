package com.jdemkiewicz.apibrowser;

import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.jdemkiewicz.apibrowser.Models.Fighter;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Optional;
import io.realm.Realm;

public class FighterDetailActivity extends AppCompatActivity {


    @BindView(R.id.fighter_detail_name)
    @Nullable
    TextView fighterName;
    @BindView(R.id.fighter_detail_image)
    @Nullable
    ImageView fighterImage;
    @BindView(R.id.fighter_wins_text)
    @Nullable
    TextView fighterWins;
    @BindView(R.id.fighter_draws_text)
    @Nullable
    TextView fighterDraws;
    @BindView(R.id.fighter_detail_loses)
    @Nullable
    TextView fighterLosses;
    @BindView(R.id.fighter_weight_text)
    @Nullable
    TextView fighterWeight;
    @BindView(R.id.fighter_status_text)
    @Nullable
    TextView fighterStatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fighter_detail);
        ButterKnife.bind(this);
        Bundle bundle = getIntent().getExtras();
//        Fighter fighter = bundle.getParcelable("fighter");
        String thumb = bundle.getString("image");
        String name = bundle.getString("name");
        String lastName = bundle.getString("lastName");
        int wins = bundle.getInt("wins");
        int losses = bundle.getInt("losses");
        int draws = bundle.getInt("draws");
        String status = bundle.getString("status");
        String weight = bundle.getString("weight");

        test(thumb, name, lastName, wins, losses, draws, status, weight);
    }

    private void initView(Fighter fighter) {
        fighterName.setText(fighter.getFirstName() + " " + fighter.getLastName());
        fighterWins.setText(String.valueOf(fighter.getWins()));
        fighterDraws.setText(String.valueOf(fighter.getDraws()));
        fighterLosses.setText(String.valueOf(fighter.getLosses()));
        fighterStatus.setText(String.valueOf(fighter.getFighterStatus()));
        Picasso.with(this)
                .load(fighter.getThumbnail())
                .resize(300, 300)
                .centerCrop()
                .into(fighterImage);
    }

    private void test(String thumb, String name, String lastName, int wins, int losses, int draw, String status, String weight) {
//        TextView textView = (TextView) findViewById(R.id.fighter_detail_name);
//        textView.setText(name);
        fighterName.setText(name + " " + lastName);
        fighterWins.setText(String.valueOf(wins));
        fighterDraws.setText(String.valueOf(draw));
        fighterLosses.setText(String.valueOf(losses));
        fighterStatus.setText(String.valueOf(status));
        fighterWeight.setText(weight);
        Picasso.with(this)
                .load(thumb)
                .into(fighterImage);
    }
}
